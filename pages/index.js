import {useState} from 'react'
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-enterprise'

import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';

export default function Home() {
  const [rowData] = useState([
    {make: "Toyota", model: "Celica", price: 35000},
    {make: "Ford", model: "Mondeo", price: 32000},
    {make: "Porsche", model: "Boxster", price: 72000}
  ]);

  const [columnDefs] = useState([
    { field: 'make' },
    { field: 'model' },
    { field: 'price' }
  ])

  return (
      <div className="ag-theme-alpine" style={{height: 600, width: 1000}}>
        <AgGridReact
            sideBar={true}
            rowData={rowData}
            columnDefs={columnDefs}>
        </AgGridReact>
      </div>
  );
}
